create table device_manufacturer (
  id serial primary key,
  name varchar(30) not null
);

create type device_type as enum ('TV', 'BDP', 'TBL', 'VGC', 'MOB');

create table device (
  id serial primary key,
  device_id varchar(80),
  type device_type,
  device_model varchar(50),
  manufacturer_id integer references device_manufacturer
);

create index manufacturer_id_fk_idx on device (manufacturer_id);
create index device_model_type_idx on device (device_model, type);

create table contract (
  contract_id integer primary key
);

create type video_type as enum ('movie', 'episode', 'season');

create table video (
  id uuid primary key,
  title varchar(80),
  russian_title varchar(80),
  type video_type,
  contract_id integer references contract
);

create index contract_id_fk_idx on video(contract_id);

create type resolution as enum ('4K', '3D', 'HD', 'SD');
create type transaction_type as enum ('purchase', 'rent', 'view');

create table transaction (
  id serial primary key,
  video_id uuid references video,
  offer_id varchar(50),
  resolution resolution,
  device_id integer references device,
  amount integer,
  type transaction_type,
  user_subscription_uid uuid
);

create index video_id_fk_idx on transaction (video_id);
create index device_id_fk_idx on transaction (device_id);