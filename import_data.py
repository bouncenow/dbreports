import csv
import psycopg2
import argparse
import sys


def process_video(cursor, row):
    video_insert = "insert into video(id, title, russian_title, type, contract_id) " \
                   "values (%(element_uid)s, %(title)s, %(russian_title)s, %(element_type)s, %(contract_id)s) " \
                   "on conflict do nothing"
    cursor.execute(video_insert, row)
    return row['element_uid']


def process_device(cursor, row):
    if row['device_type'] == 'WEB':
        return None
    else:
        manufacturer_id = process_manufacturer(cursor, row)
    get_existing = "select id from device where device_id = %(device_id)s"
    cursor.execute(get_existing, row)
    existing_id = cursor.fetchone()
    if existing_id is not None:
        return existing_id[0]
    else:
        device_insert = "insert into device(device_id, type, device_model, manufacturer_id) " \
                        "values (%(device_id)s, %(device_type)s, %(device_model)s, %(manufacturer_id)s) " \
                        "returning device.id"
        params = {}
        params.update(row)
        params.update({'manufacturer_id': manufacturer_id})
        cursor.execute(device_insert, params)
        return cursor.fetchone()[0]


def process_manufacturer(cursor, row):
    get_existing = "select id from device_manufacturer where name = %(device_manufacturer)s"
    cursor.execute(get_existing, row)
    existing_id = cursor.fetchone()
    if existing_id is not None:
        return existing_id[0]
    else:
        manufacturer_insert = "insert into device_manufacturer(name) " \
                              "values (%(device_manufacturer)s) " \
                              "returning device_manufacturer.id"
        cursor.execute(manufacturer_insert, row)
        return cursor.fetchone()[0]


def process_contract(cursor, row):
    contract_insert = "insert into contract values (%(contract_id)s) on conflict do nothing"
    cursor.execute(contract_insert, row)


def get_transaction_type(row):
    if row['tx_type'] == 'view':
        return 'view'
    else:
        return row['purchase_type']


def process_transaction(cursor, row, video_id, device_id):
    transaction_insert = "insert into transaction(video_id, offer_id, resolution, device_id, amount, " \
                         "type, user_subscription_uid) values " \
                         "(%(video_id)s, %(offer_id)s, %(resolution)s, %(device_id)s, %(tx_amount)s, " \
                         "%(transaction_type)s, %(user_subscription_uid)s)"
    transaction_type = get_transaction_type(row)
    params = {}
    params.update(row)
    params.update({'video_id': video_id, 'device_id': device_id, 'transaction_type': transaction_type})
    if params['user_subscription_uid'] == '':
        params['user_subscription_uid'] = None
    cursor.execute(transaction_insert, params)


def process(conn, cursor, row):
    process_contract(cursor, row)
    video_id = process_video(cursor, row)
    device_id = process_device(cursor, row)
    process_transaction(cursor, row, video_id, device_id)
    conn.commit()


def import_sample(file_name, dbhost, dbname, dbuser, dbpassword, ssl):
    connect_str = "dbname='{}' user='{}' host='{}' password='{}'".format(dbname,
                                                                         dbuser,
                                                                         dbhost,
                                                                         dbpassword)
    if ssl:
        connect_str += " sslmode='require'"
    print(connect_str)
    with open(file_name, mode='r', newline='', encoding='utf-8-sig') as infile:
        reader = csv.DictReader(infile, delimiter='\t')
        with psycopg2.connect(connect_str) as db_connection:
            with db_connection.cursor() as db_cursor:
                for (i, file_row) in enumerate(reader):
                    process(db_connection, db_cursor, file_row)
                    if i % 500 == 0:
                        print("Processed {} records...".format(i + 1))


if __name__ == "__main__":
    parser = argparse.ArgumentParser(description='Import data sample')
    parser.add_argument('--file_name', required=True)
    parser.add_argument('--dbhost', required=True)
    parser.add_argument('--dbname', required=True)
    parser.add_argument('--dbuser', required=True)
    parser.add_argument('--dbpassword', required=True)
    parser.add_argument('--ssl', action='store_true', default=False)
    args = parser.parse_args()
    import_sample(**vars(args))
