<#-- @ftlvariable name="contracts" type="java.util.List<org.bitbucket.bouncenow.dbreports.model.video.ContractOverallStatistic>" -->
<#import "defaultTemplate.ftl" as layout>
<@layout.default>
<div class="row">
    <div class="col-md-4">
        <h2>Manufacturers</h2>
        <p><a class="btn btn-default" href="/manufacturers" role="button">View &raquo;</a></p>
    </div>
    <div class="col-md-4">
        <h2>Contracts</h2>
        <p><a class="btn btn-default" href="/contracts" role="button">View &raquo;</a></p>
    </div>
</div>
</@layout.default>