<#-- @ftlvariable name="statistic" type="org.bitbucket.bouncenow.dbreports.model.video.ContractOverallStatistic" -->
<#import "defaultTemplate.ftl" as layout>
<@layout.default>
<div class="row">
    <h2>
        Contract id: ${statistic.contractId}
    </h2>
</div>
<div class="row">
    <table class="table">
        <thead>
        <tr>
            <th>Video title</th>
            <th>Video type</th>
            <th>Views</th>
            <th>Purchases</th>
            <th>Purchases sum</th>
            <th>Rents</th>
            <th>Rent sum</th>
        </tr>
        </thead>
        <tbody>
            <#list statistic.videos as video_stat>
            <tr>
                <td>
                    <a href="/videos/${video_stat.video.uid}">${video_stat.video.title}</a>
                </td>
                <td>${video_stat.video.type}</td>
                <td>${video_stat.viewCount}</td>
                <td>${video_stat.purchaseCount}</td>
                <td>${video_stat.purchaseSum}</td>
                <td>${video_stat.rentCount}</td>
                <td>${video_stat.rentSum}</td>
            </tr>
            </#list>
            <tr>
                <td colspan="3">Total:</td>
                <td>${statistic.purchaseTotalCount}</td>
                <td>${statistic.purchaseTotalSum}</td>
                <td>${statistic.rentTotalCount}</td>
                <td>${statistic.rentTotalSum}</td>
            </tr>
        </tbody>
    </table>
</div>
</@layout.default>