<#-- @ftlvariable name="error" type="String" -->
<#import "defaultTemplate.ftl" as layout>
<@layout.default>
<div class="row">
    <div class="col-md-4">
        <h2>Error:</h2>
        <p>${error}</p>
    </div>
</div>
</@layout.default>