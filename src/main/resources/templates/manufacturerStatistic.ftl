<#-- @ftlvariable name="statistic" type="org.bitbucket.bouncenow.dbreports.model.device.DeviceManufacturerOverallStatistic" -->
<#import "defaultTemplate.ftl" as layout>
<@layout.default>
<div сlass="row">
    <h2>
        <#if statistic.manufacturer??>
            ${statistic.manufacturer.name}
        <#else>
            "Unknown/WEB"
        </#if>
    </h2>
    <table class="table">
        <thead>
        <tr>
            <#if statistic.manufacturer??>
                <th>Device model</th>
                <th>Device type</th>
            </#if>
            <th>Purchases</th>
            <th>Rents</th>
            <th>Subscriptions</th>
            <th>Views</th>
        </tr>
        </thead>
        <tbody>
            <#list statistic.deviceStatistics as device_stats>
                <tr>
                    <#if device_stats.device??>
                        <td>${device_stats.device.model}</td>
                        <td>${device_stats.device.type}</td>
                    </#if>
                    <td>${device_stats.purchaseCount}</td>
                    <td>${device_stats.rentCount}</td>
                    <td>${device_stats.subscriptionCount}</td>
                    <td>${device_stats.viewCount}</td>
                </tr>
            </#list>
        </tbody>
    </table>
</div>
</@layout.default>