<#-- @ftlvariable name="combinations" type="java.util.List<org.bitbucket.bouncenow.dbreports.model.video.VideoTransactionCombination>" -->
<table class="table">
    <thead>
    <tr>
        <th>Resolution</th>
        <th>Price</th>
        <th>Offer id</th>
        <th>Count</th>
    </tr>
    </thead>
    <tbody>
        <#list combinations as combination>
        <tr>
            <td>${combination.resolution}</td>
            <td>${combination.price}</td>
            <td>${combination.offerId}</td>
            <td>${combination.count}</td>
        </tr>
        </#list>
    </tbody>
</table>