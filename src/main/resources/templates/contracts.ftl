<#-- @ftlvariable name="contracts" type="java.util.List<org.bitbucket.bouncenow.dbreports.model.video.Contract>" -->
<#import "defaultTemplate.ftl" as layout>
<@layout.default>
<div class="row">
    <div class="col-md-2">
        <ul class="list-group">
            <#list contracts as contract>
                <li class="list-group-item">
                    <a href="/contracts/#{contract.id}">
                        ${contract.id}
                    </a>
                </li>
            </#list>
        </ul>
    </div>
</div>
</@layout.default>