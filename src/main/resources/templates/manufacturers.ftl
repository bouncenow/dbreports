<#-- @ftlvariable name="manufacturers" type="java.util.List<org.bitbucket.bouncenow.dbreports.model.device.DeviceManufacturer>" -->
<#import "defaultTemplate.ftl" as layout>
<@layout.default>
<div class="row">
    <div class="col-md-3">
        <ul class="list-group">
            <li class="list-group-item">
                <a href="/unknownManufacturer">Unknown/WEB</a>
            </li>
            <#list manufacturers as manufacturer>
                <li class="list-group-item">
                    <a href="/manufacturers/#{manufacturer.id}">
                        ${manufacturer.name}
                    </a>
                </li>
            </#list>
        </ul>
    </div>
</div>
</@layout.default>