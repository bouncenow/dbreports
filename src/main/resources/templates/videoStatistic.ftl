<#-- @ftlvariable name="statistic" type="org.bitbucket.bouncenow.dbreports.model.video.VideoCombinationsStatistic" -->
<#import "defaultTemplate.ftl" as layout>
<@layout.default>
<div class="row">
    <div>
        <h2>${statistic.video.title}</h2>
        <h2>${statistic.video.type}</h2>
    </div>
</div>
<#if statistic.purchaseCombinations?has_content>
<div class="row">
    <h3>Purchases:</h3>
    <#assign combinations = statistic.purchaseCombinations>
    <#include "videoCombinations.ftl">
</div>
</#if>
<#if statistic.rentCombinations?has_content>
<div class="row">
    <h3>Rents:</h3>
    <#assign combinations = statistic.rentCombinations>
    <#include "videoCombinations.ftl">
</div>
</#if>
<#if statistic.subscriptionCombinations?has_content>
<div class="row">
    <h3>Subscriptions</h3>
    <#assign combinations = statistic.subscriptionCombinations>
    <#include "videoCombinations.ftl">
</div>
</#if>
</@layout.default>