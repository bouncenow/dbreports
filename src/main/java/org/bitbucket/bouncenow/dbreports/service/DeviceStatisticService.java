package org.bitbucket.bouncenow.dbreports.service;

import org.bitbucket.bouncenow.dbreports.exception.NotFoundException;
import org.bitbucket.bouncenow.dbreports.model.device.DeviceManufacturer;
import org.bitbucket.bouncenow.dbreports.model.device.DeviceManufacturerOverallStatistic;
import org.bitbucket.bouncenow.dbreports.model.device.DeviceOverallStatistic;
import org.bitbucket.bouncenow.dbreports.repository.DeviceStatisticRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.Collections;
import java.util.List;
import java.util.Optional;

@Service
public class DeviceStatisticService {

    private final DeviceStatisticRepository deviceStatisticRepository;

    @Autowired
    public DeviceStatisticService(DeviceStatisticRepository deviceStatisticRepository) {
        this.deviceStatisticRepository = deviceStatisticRepository;
    }

    public List<DeviceManufacturer> getAllDeviceManufacturer() {
        return deviceStatisticRepository.getAllDeviceManufacturers();
    }

    public DeviceManufacturerOverallStatistic getDeviceManufacturerOverallStatistic(long id) {
        Optional<DeviceManufacturer> deviceManufacturer = deviceStatisticRepository.getDeviceManufacturerById(id);
        return deviceManufacturer
                .map(this::buildManufacturerOverallStatistic)
                .orElseThrow(NotFoundException::new);
    }

    public DeviceManufacturerOverallStatistic getUnknownManufacturerOverallStatistic() {
        DeviceOverallStatistic unknownDeviceStatistic = deviceStatisticRepository.getStatisticForUnknownDevice();

        DeviceManufacturerOverallStatistic overallStatistic = new DeviceManufacturerOverallStatistic();
        overallStatistic.setDeviceStatistics(Collections.singletonList(unknownDeviceStatistic));

        return overallStatistic;
    }

    private DeviceManufacturerOverallStatistic buildManufacturerOverallStatistic(DeviceManufacturer m) {
        DeviceManufacturerOverallStatistic manufacturerOverallStatistic = new DeviceManufacturerOverallStatistic();
        manufacturerOverallStatistic.setDeviceStatistics(deviceStatisticRepository.getStatisticByDevicesForManufacturer(m.getId()));
        manufacturerOverallStatistic.setManufacturer(m);
        return manufacturerOverallStatistic;
    }
}
