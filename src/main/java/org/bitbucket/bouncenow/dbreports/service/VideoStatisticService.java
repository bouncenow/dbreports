package org.bitbucket.bouncenow.dbreports.service;

import org.bitbucket.bouncenow.dbreports.exception.NotFoundException;
import org.bitbucket.bouncenow.dbreports.model.video.*;
import org.bitbucket.bouncenow.dbreports.repository.VideoStatisticRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.Objects;
import java.util.Optional;
import java.util.UUID;
import java.util.function.Function;
import java.util.stream.Collectors;

@Service
public class VideoStatisticService {

    private final VideoStatisticRepository repository;

    @Autowired
    public VideoStatisticService(VideoStatisticRepository repository) {
        this.repository = repository;
    }

    public List<Contract> getAllContracts() {
        return repository.getAllContracts();
    }

    public ContractOverallStatistic getContractOverallStatistic(long id) {
        return repository.getContractById(id)
                .map(this::buildContractOverallStatistic)
                .orElseThrow(NotFoundException::new);
    }

    public VideoCombinationsStatistic getVideoCombinationsStatistic(String uid) {

        return Optional.ofNullable(uid)
                .map(this::transformToUuid)
                .flatMap(repository::getVideoById)
                .map(this::buildVideoCombinationsStatistic)
                .orElseThrow(NotFoundException::new);
    }

    private UUID transformToUuid(String uid) {
        try {
            return UUID.fromString(uid);
        } catch (IllegalArgumentException e) {
            return null;
        }
    }

    private int getSumByField(List<VideoOverallStatistic> statistics,
                              Function<? super VideoOverallStatistic, ? extends Integer> fieldAccessor) {
        return statistics
                .stream()
                .map(fieldAccessor)
                .mapToInt(Integer::intValue)
                .sum();
    }

    private ContractOverallStatistic buildContractOverallStatistic(Contract c) {
        ContractOverallStatistic statistic = new ContractOverallStatistic();
        statistic.setContractId(c.getId());
        List<VideoOverallStatistic> videoStatistics = repository
                .getContractVideosOverallStatistics(c.getId());

        int rentTotalCount = getSumByField(videoStatistics,
                VideoOverallStatistic::getRentCount);
        int rentTotalSum = getSumByField(videoStatistics,
                VideoOverallStatistic::getRentSum);
        int purchaseTotalCount = getSumByField(videoStatistics,
                VideoOverallStatistic::getPurchaseCount);
        int purchaseTotalSum = getSumByField(videoStatistics,
                VideoOverallStatistic::getPurchaseSum);
        int viewTotalCount = getSumByField(videoStatistics,
                VideoOverallStatistic::getViewCount);

        statistic.setVideos(videoStatistics);
        statistic.setPurchaseTotalCount(purchaseTotalCount);
        statistic.setPurchaseTotalSum(purchaseTotalSum);
        statistic.setRentTotalCount(rentTotalCount);
        statistic.setRentTotalSum(rentTotalSum);
        statistic.setViewTotalCount(viewTotalCount);

        return statistic;
    }

    private VideoCombinationsStatistic buildVideoCombinationsStatistic(Video v) {
        VideoCombinationsStatistic statistic = new VideoCombinationsStatistic();
        statistic.setVideo(v);
        List<VideoTransactionCombination> combinations =
                repository.getVideoTransactionCombinations(v.getUid());

        List<VideoTransactionCombination> rents =
                filterWithTransactionType(combinations, TransactionType.RENT);
        List<VideoTransactionCombination> purchases =
                filterWithTransactionType(combinations, TransactionType.PURCHASE);
        List<VideoTransactionCombination> subscriptions =
                filterWithTransactionType(combinations, TransactionType.SUBSCRIPTION_VIEW);

        statistic.setPurchaseCombinations(purchases);
        statistic.setRentCombinations(rents);
        statistic.setSubscriptionCombinations(subscriptions);

        return statistic;
    }

    private List<VideoTransactionCombination> filterWithTransactionType(List<VideoTransactionCombination> combinations, TransactionType type) {
        return combinations
                .stream()
                .filter(c -> Objects.equals(type, c.getTransactionType()))
                .collect(Collectors.toList());
    }
}
