package org.bitbucket.bouncenow.dbreports.model.video;

import lombok.Data;

import java.util.UUID;

@Data
public class Video {

    private UUID uid;

    private VideoType type;

    private String title;

    private String russianTitle;

}
