package org.bitbucket.bouncenow.dbreports.model.device;

import lombok.Data;

@Data
public class DeviceManufacturer {

    private Long id;

    private String name;

}
