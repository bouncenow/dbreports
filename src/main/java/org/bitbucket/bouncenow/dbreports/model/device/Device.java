package org.bitbucket.bouncenow.dbreports.model.device;

import lombok.Data;

@Data
public class Device {

    private String model;

    private DeviceType type;

}
