package org.bitbucket.bouncenow.dbreports.model.video;

import lombok.Data;

import java.util.ArrayList;
import java.util.List;

@Data
public class VideoCombinationsStatistic {

    private Video video;

    private List<VideoTransactionCombination> purchaseCombinations = new ArrayList<>();

    private List<VideoTransactionCombination> rentCombinations = new ArrayList<>();

    private List<VideoTransactionCombination> subscriptionCombinations = new ArrayList<>();

}
