package org.bitbucket.bouncenow.dbreports.model.video;

import lombok.Data;

@Data
public class VideoOverallStatistic {

    private Video video;

    private int purchaseCount;
    private int purchaseSum;

    private int rentCount;
    private int rentSum;

    private int viewCount;

}
