package org.bitbucket.bouncenow.dbreports.model.device;

import lombok.Data;

@Data
public class DeviceOverallStatistic {

    private Device device;

    private int purchaseCount;
    private int rentCount;
    private int viewCount;
    private int subscriptionCount;

}
