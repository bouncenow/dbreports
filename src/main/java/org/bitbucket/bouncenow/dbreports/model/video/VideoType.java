package org.bitbucket.bouncenow.dbreports.model.video;

import java.util.Arrays;

public enum VideoType {

    MOVIE("movie"), SEASON("season"), EPISODE("episode");


    private final String value;

    VideoType(String value) {
        this.value = value;
    }

    public static VideoType findByValue(String value) {
        return Arrays
                .stream(values())
                .filter(t -> t.value.equals(value))
                .findFirst()
                .orElseThrow(IllegalArgumentException::new);
    }
}
