package org.bitbucket.bouncenow.dbreports.model.device;

import lombok.Data;

import java.util.ArrayList;
import java.util.List;

@Data
public class DeviceManufacturerOverallStatistic {

    private DeviceManufacturer manufacturer;

    private List<DeviceOverallStatistic> deviceStatistics = new ArrayList<>();

}
