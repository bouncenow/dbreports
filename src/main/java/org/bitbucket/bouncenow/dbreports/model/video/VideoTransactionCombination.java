package org.bitbucket.bouncenow.dbreports.model.video;

import lombok.Data;

@Data
public class VideoTransactionCombination {

    private Resolution resolution;

    private int price;

    private String offerId;

    private int count;

    private TransactionType transactionType;

}
