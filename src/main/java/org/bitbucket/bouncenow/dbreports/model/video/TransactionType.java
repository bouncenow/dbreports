package org.bitbucket.bouncenow.dbreports.model.video;

import java.util.Arrays;

public enum TransactionType {

    PURCHASE("purchase"), RENT("rent"), SUBSCRIPTION_VIEW("view");

    private final String value;

    TransactionType(String value) {
        this.value = value;
    }

    public static TransactionType findByValue(String value) {
        return Arrays
                .stream(values())
                .filter(t -> t.value.equals(value))
                .findFirst()
                .orElseThrow(IllegalArgumentException::new);
    }

}
