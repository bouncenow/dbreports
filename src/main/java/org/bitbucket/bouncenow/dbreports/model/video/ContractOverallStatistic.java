package org.bitbucket.bouncenow.dbreports.model.video;

import lombok.Data;

import java.util.ArrayList;
import java.util.List;

@Data
public class ContractOverallStatistic {

    private long contractId;

    private List<VideoOverallStatistic> videos = new ArrayList<>();

    private int rentTotalSum;
    private int rentTotalCount;

    private int purchaseTotalSum;
    private int purchaseTotalCount;

    private int viewTotalCount;

}
