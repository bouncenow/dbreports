package org.bitbucket.bouncenow.dbreports.model.video;

import java.util.Arrays;

public enum Resolution {

    SD("SD"), FOUR_K("4K"), HD("HD"), THREE_D("3D");

    private final String value;

    Resolution(String value) {
        this.value = value;
    }

    public static Resolution findByValue(String value) {
        return Arrays
                .stream(values())
                .filter(r -> r.value.equals(value))
                .findFirst()
                .orElseThrow(IllegalArgumentException::new);
    }
}
