package org.bitbucket.bouncenow.dbreports.model.device;

import java.util.Arrays;

public enum DeviceType {

    TV("TV"), BDP("BDP"), TABLET("TBL"), VIDEO_GAME_CONSOLE("VGC"), MOBILE("MOB");

    private final String value;

    DeviceType(String value) {
        this.value = value;
    }

    public static DeviceType getByValue(String value) {
        return Arrays
                .stream(values())
                .filter(d -> d.value.equals(value))
                .findFirst()
                .orElseThrow(IllegalArgumentException::new);
    }

}
