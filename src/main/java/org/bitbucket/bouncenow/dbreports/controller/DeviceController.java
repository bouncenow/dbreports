package org.bitbucket.bouncenow.dbreports.controller;

import org.bitbucket.bouncenow.dbreports.service.DeviceStatisticService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;

@Controller
public class DeviceController {

    private static final String MANUFACTURER_STATISTIC_VIEW = "manufacturerStatistic";
    private static final String MANUFACTURERS_VIEW = "manufacturers";

    private final DeviceStatisticService service;

    @Autowired
    public DeviceController(DeviceStatisticService service) {
        this.service = service;
    }

    @RequestMapping("/manufacturers")
    public String manufacturers(Model model) {
        model.addAttribute("manufacturers",
                service.getAllDeviceManufacturer());

        return MANUFACTURERS_VIEW;
    }

    @RequestMapping("/unknownManufacturer")
    public String unknownManufacturer(Model model) {
        model.addAttribute("statistic",
                service.getUnknownManufacturerOverallStatistic());
        return MANUFACTURER_STATISTIC_VIEW;
    }

    @RequestMapping("/manufacturers/{id}")
    public String manufacturer(@PathVariable long id, Model model) {
        model.addAttribute("statistic",
                service.getDeviceManufacturerOverallStatistic(id));

        return MANUFACTURER_STATISTIC_VIEW;
    }

}
