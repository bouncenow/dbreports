package org.bitbucket.bouncenow.dbreports.controller;

import org.bitbucket.bouncenow.dbreports.service.VideoStatisticService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;

@Controller
public class VideoController {

    private final VideoStatisticService service;

    @Autowired
    public VideoController(VideoStatisticService service) {
        this.service = service;
    }

    @RequestMapping("/contracts")
    public String contracts(Model model) {

        model.addAttribute("contracts", service.getAllContracts());

        return "contracts";
    }

    @RequestMapping("/contracts/{id}")
    public String contractStatistic(@PathVariable long id, Model model) {

        model.addAttribute("statistic", service.getContractOverallStatistic(id));

        return "contractStatistic";
    }

    @RequestMapping("/videos/{uid}")
    public String videoStatistic(@PathVariable String uid, Model model) {

        model.addAttribute("statistic", service.getVideoCombinationsStatistic(uid));

        return "videoStatistic";
    }
}
