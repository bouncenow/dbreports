package org.bitbucket.bouncenow.dbreports.controller;

import org.bitbucket.bouncenow.dbreports.exception.NotFoundException;
import org.springframework.web.bind.annotation.ControllerAdvice;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.servlet.ModelAndView;

import javax.servlet.http.HttpServletRequest;

@ControllerAdvice
public class GlobalExceptionHandlingAdvice {

    @ExceptionHandler(value = NotFoundException.class)
    public ModelAndView notFound(HttpServletRequest request, Exception e) {
        ModelAndView modelAndView = new ModelAndView("error");
        modelAndView.addObject("error", "Page not found");
        return modelAndView;
    }

}
