package org.bitbucket.bouncenow.dbreports.repository;

import org.bitbucket.bouncenow.dbreports.model.device.*;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.jdbc.core.namedparam.NamedParameterJdbcTemplate;
import org.springframework.stereotype.Repository;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.Collections;
import java.util.List;
import java.util.Map;
import java.util.Optional;

@Repository
public class DeviceStatisticRepository {

    private static final String GET_ALL_DEVICE_MANUFACTURERS_QUERY =
            "select m.id id, m.name manufacturer_name " +
                    "from device_manufacturer m";

    private static final String GET_DEVICE_MANUFACTURER_BY_ID_QUERY =
            "select m.id id, m.name manufacturer_name " +
                    "from device_manufacturer m " +
                    "where m.id = :id";
    
    private static final String GET_DEVICE_MANUFACTURER_OVERALL_STATISTIC_QUERY = 
            "with devices as ( " +
                    "    select " +
                    "      d.id           id, " +
                    "      m.name         manufacturer, " +
                    "      d.device_model model, " +
                    "      d.type         device_type " +
                    "    from device_manufacturer m " +
                    "      join device d on m.id = d.manufacturer_id " +
                    "    where m.id = :manufacturer_id " +
                    "), purchase as ( " +
                    "    select " +
                    "      d.id     id, " +
                    "      count(*) count " +
                    "    from devices d " +
                    "      join transaction t on d.id = t.device_id " +
                    "    where t.type = 'purchase' " +
                    "    group by d.id " +
                    "), rent as ( " +
                    "    select " +
                    "      d.id     id, " +
                    "      count(*) count " +
                    "    from devices d " +
                    "      join transaction t on d.id = t.device_id " +
                    "    where t.type = 'rent' " +
                    "    group by d.id " +
                    "), view as ( " +
                    "    select " +
                    "      d.id     id, " +
                    "      count(*) count " +
                    "    from devices d " +
                    "      join transaction t on d.id = t.device_id " +
                    "    where t.type = 'view' " +
                    "    group by d.id " +
                    "), subscription as ( " +
                    "    select " +
                    "      d.id id, " +
                    "      count(distinct t.user_subscription_uid) " +
                    "    from devices d " +
                    "      join transaction t on d.id = t.device_id " +
                    "    group by d.id " +
                    ") " +
                    "select " +
                    "  d.model       model, " +
                    "  d.device_type device_type, " +
                    "  sum(p.count)  purchase_count, " +
                    "  sum(r.count)  rent_count, " +
                    "  sum(v.count)  view_count, " +
                    "  sum(s.count)  subscription_count " +
                    "from devices d " +
                    "  left join purchase p on d.id = p.id " +
                    "  left join rent r on d.id = r.id " +
                    "  left join view v on d.id = v.id " +
                    "  left join subscription s on d.id = s.id " +
                    "group by d.model, d.device_type";
    
    private static final String GET_UNKNOWN_MANUFACTURER_OVERALL_STATISTICS_QUERY = 
            "with transactions_no_manufacturer as ( " +
                    "    select t.type, t.user_subscription_uid " +
                    "    from transaction t " +
                    "    where t.device_id is null " +
                    "), purchase as ( " +
                    "    select count(*) count " +
                    "    from transactions_no_manufacturer t " +
                    "    where t.type = 'purchase' " +
                    "), rent as ( " +
                    "    select count(*) count " +
                    "    from transactions_no_manufacturer t " +
                    "    where t.type = 'rent' " +
                    "), subscription as ( " +
                    "    select count(distinct t.user_subscription_uid) count " +
                    "    from transactions_no_manufacturer t " +
                    "), view as ( " +
                    "    select count(*) count " +
                    "    from transactions_no_manufacturer t " +
                    "    where t.type = 'view' " +
                    ") " +
                    "select " +
                    "  p.count purchase_count, " +
                    "  r.count rent_count, " +
                    "  v.count view_count, " +
                    "  s.count subscription_count " +
                    "from purchase p, rent r, view v, subscription s";

    private final NamedParameterJdbcTemplate template;

    @Autowired
    public DeviceStatisticRepository(NamedParameterJdbcTemplate template) {
        this.template = template;
    }

    private static DeviceManufacturer mapDeviceManufacturer(ResultSet rs, int rowNum) {
        DeviceManufacturer manufacturer = new DeviceManufacturer();
        try {
            manufacturer.setId(rs.getLong("id"));
            manufacturer.setName(rs.getString("manufacturer_name"));
        } catch (SQLException e) {
            throw new IllegalArgumentException("SQL exception during matching row for device_manufacturer", e);
        }
        return manufacturer;
    }

    private static DeviceOverallStatistic mapDeviceStatistic(ResultSet rs, int rowNum) {
        try {
            DeviceOverallStatistic deviceStatistic = mapBaseDeviceStatistic(rs, rowNum);
            Device device = new Device();
            device.setModel(rs.getString("model"));
            device.setType(DeviceType.getByValue(rs.getString("device_type")));
            deviceStatistic.setDevice(device);
            return deviceStatistic;
        } catch (SQLException e) {
            throw new IllegalArgumentException("SQL exception during matching row for device statistic", e);
        }
    }

    private static DeviceOverallStatistic mapBaseDeviceStatistic(ResultSet rs, int rowNum) {
        try {
            DeviceOverallStatistic deviceStatistic = new DeviceOverallStatistic();
            deviceStatistic.setPurchaseCount(rs.getInt("purchase_count"));
            deviceStatistic.setRentCount(rs.getInt("rent_count"));
            deviceStatistic.setViewCount(rs.getInt("view_count"));
            deviceStatistic.setSubscriptionCount(rs.getInt("subscription_count"));
            return deviceStatistic;
        } catch (SQLException e) {
            throw new IllegalArgumentException("SQL exception during matching row for device statistic", e);
        }
    }


    public List<DeviceManufacturer> getAllDeviceManufacturers() {
        return template.query(GET_ALL_DEVICE_MANUFACTURERS_QUERY,
                DeviceStatisticRepository::mapDeviceManufacturer);
    }

    public Optional<DeviceManufacturer> getDeviceManufacturerById(long id) {
        Map<String, Long> param = Collections.singletonMap("id", id);
        List<DeviceManufacturer> deviceManufacturersWithId =
                template.query(GET_DEVICE_MANUFACTURER_BY_ID_QUERY,
                        param, DeviceStatisticRepository::mapDeviceManufacturer);
        return deviceManufacturersWithId
                .stream()
                .findFirst();
    }

    public List<DeviceOverallStatistic> getStatisticByDevicesForManufacturer(long id) {
        Map<String, Long> param = Collections.singletonMap("manufacturer_id", id);
        return template.query(GET_DEVICE_MANUFACTURER_OVERALL_STATISTIC_QUERY,
                param, DeviceStatisticRepository::mapDeviceStatistic);
    }

    public DeviceOverallStatistic getStatisticForUnknownDevice() {
        return template.query(GET_UNKNOWN_MANUFACTURER_OVERALL_STATISTICS_QUERY,
                DeviceStatisticRepository::mapBaseDeviceStatistic)
                .stream()
                .findFirst()
                .orElseThrow(IllegalStateException::new);
    }

}
