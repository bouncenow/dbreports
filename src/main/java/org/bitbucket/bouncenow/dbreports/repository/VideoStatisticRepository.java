package org.bitbucket.bouncenow.dbreports.repository;

import org.bitbucket.bouncenow.dbreports.model.video.*;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.jdbc.core.namedparam.NamedParameterJdbcTemplate;
import org.springframework.stereotype.Repository;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.*;

@Repository
public class VideoStatisticRepository {

    private static final String GET_CONTRACT_BY_ID_QUERY =
            "select c.contract_id id " +
                    "from contract c " +
                    "where c.contract_id = :contract_id";

    private static final String GET_ALL_CONTRACTS_QUERY =
            "select c.contract_id id " +
                    "from contract c";

    private static final String GET_VIDEO_BY_ID =
            "select v.id video_uid, " +
                    "v.title title, " +
                    "v.russian_title russian_title, " +
                    "v.type video_type " +
                    "from video v " +
                    "where v.id = :uid";

    private static final String GET_ALL_VIDEO_TRANSCATION_COMBINATIONS_QUERY =
            "select " +
                    "  v.id video_uid, " +
                    "  v.title title, " +
                    "  t.resolution resolution, " +
                    "  t.amount price, " +
                    "  t.offer_id offer_id, " +
                    "  t.type transaction_type, " +
                    "  count(*) count " +
                    "from video v " +
                    "  join transaction t on v.id = t.video_id " +
                    "where v.id = :video_uid " +
                    "group by v.id, t.resolution, t.amount, t.offer_id, t.type";

    private static final String GET_CONTRACT_VIDEOS_OVERALL_STATISTIC_QUERY =
            "with contract_videos as ( " +
                    "    select " +
                    "      v.id    id, " +
                    "      v.title title, " +
                    "      v.russian_title russian_title, " +
                    "      v.type video_type" +
                    "    from video v " +
                    "      join contract c1 on v.contract_id = c1.contract_id " +
                    "    where c1.contract_id = :contract_id " +
                    "), videos_rent as ( " +
                    "    select " +
                    "      v.id          id, " +
                    "      count(t.type) rent_count, " +
                    "      sum(t.amount) rent_sum " +
                    "    from contract_videos v " +
                    "      join transaction t on t.video_id = v.id " +
                    "    where t.type = 'rent' " +
                    "    group by v.id " +
                    "), videos_purchase as ( " +
                    "    select " +
                    "      v.id          id, " +
                    "      count(t.type) purchase_count, " +
                    "      sum(t.amount) purchase_sum " +
                    "    from contract_videos v " +
                    "      join transaction t on t.video_id = v.id " +
                    "    where t.type = 'purchase' " +
                    "    group by v.id " +
                    "), videos_view as ( " +
                    "    select " +
                    "      v.id          id, " +
                    "      count(t.type) view_count " +
                    "    from contract_videos v " +
                    "      join transaction t on t.video_id = v.id " +
                    "    where t.type = 'view' " +
                    "    group by v.id " +
                    ") " +
                    "select " +
                    "  v.id video_uid, " +
                    "  v.title title, " +
                    "  v.russian_title russian_title, " +
                    "  v.video_type video_type, " +
                    "  r.rent_count rent_count, " +
                    "  r.rent_sum rent_sum, " +
                    "  p.purchase_count purchase_count, " +
                    "  p.purchase_sum purchase_sum, " +
                    "  v_v.view_count view_count " +
                    "from contract_videos v " +
                    "  left join videos_rent r on v.id = r.id " +
                    "  left join videos_purchase p on v.id = p.id " +
                    "  left join videos_view v_v on v.id = v_v.id ";

    private final NamedParameterJdbcTemplate template;

    @Autowired
    public VideoStatisticRepository(NamedParameterJdbcTemplate template) {
        this.template = template;
    }

    private static Contract mapContract(ResultSet rs, int rowNum) {
        Contract contract = new Contract();
        try {
            contract.setId(rs.getLong("id"));
        } catch (SQLException e) {
            throw new IllegalArgumentException("SQL exception during matching row for contract");
        }
        return contract;
    }

    private static VideoTransactionCombination mapVideoTransactionCombination(ResultSet rs, int rowNum) {
        try {
            VideoTransactionCombination combination = new VideoTransactionCombination();
            combination.setOfferId(rs.getString("offer_id"));
            combination.setPrice(rs.getInt("price"));
            combination.setResolution(Resolution.findByValue(rs.getString("resolution")));
            combination.setCount(rs.getInt("count"));
            combination.setTransactionType(TransactionType.findByValue(rs.getString("transaction_type")));
            return combination;
        } catch (SQLException e) {
            throw new IllegalArgumentException("SQL exception during matching row for video transaction combination");
        }
    }

    private static VideoOverallStatistic mapVideoOverallStatistic(ResultSet rs, int rowNum) {
        try {
            VideoOverallStatistic videoOverallStatistic = new VideoOverallStatistic();
            Video video = mapVideo(rs, rowNum);
            videoOverallStatistic.setVideo(video);
            videoOverallStatistic.setPurchaseCount(rs.getInt("purchase_count"));
            videoOverallStatistic.setPurchaseSum(rs.getInt("purchase_sum"));
            videoOverallStatistic.setRentCount(rs.getInt("rent_count"));
            videoOverallStatistic.setRentSum(rs.getInt("rent_sum"));
            videoOverallStatistic.setViewCount(rs.getInt("view_count"));
            return videoOverallStatistic;
        } catch (SQLException e) {
            throw new IllegalArgumentException("SQL exception during matching row for video overall statistic");
        }
    }

    private static Video mapVideo(ResultSet rs, int rowNum) {
        try {
            Video video = new Video();
            video.setUid((UUID) rs.getObject("video_uid"));
            video.setTitle(rs.getString("title"));
            video.setRussianTitle(rs.getString("russian_title"));
            video.setType(VideoType.findByValue(rs.getString("video_type")));
            return video;
        } catch (SQLException e) {
            throw new IllegalArgumentException("SQL exception during matching row for video", e);
        }
    }

    public Optional<Video> getVideoById(UUID uid) {
        Map<String, UUID> param = Collections.singletonMap("uid", uid);
        return template.query(GET_VIDEO_BY_ID, param, VideoStatisticRepository::mapVideo)
                .stream()
                .findFirst();
    }

    public List<Contract> getAllContracts() {
        return template.query(GET_ALL_CONTRACTS_QUERY, VideoStatisticRepository::mapContract);
    }

    public Optional<Contract> getContractById(long id) {
        Map<String, Long> param = Collections.singletonMap("contract_id", id);
        return template.query(GET_CONTRACT_BY_ID_QUERY, param, VideoStatisticRepository::mapContract)
                .stream()
                .findFirst();
    }

    public List<VideoTransactionCombination> getVideoTransactionCombinations(UUID videoUid) {
        Map<String, UUID> param = Collections.singletonMap("video_uid", videoUid);
        return template.query(GET_ALL_VIDEO_TRANSCATION_COMBINATIONS_QUERY, param, VideoStatisticRepository::mapVideoTransactionCombination);
    }

    public List<VideoOverallStatistic> getContractVideosOverallStatistics(long contractId) {
        Map<String, Long> param = Collections.singletonMap("contract_id", contractId);
        return template.query(GET_CONTRACT_VIDEOS_OVERALL_STATISTIC_QUERY, param, VideoStatisticRepository::mapVideoOverallStatistic);
    }
}
