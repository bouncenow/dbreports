package org.bitbucket.bouncenow.dbreports;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class DbReportsApplication {

    public static void main(String[] args) {
        SpringApplication.run(DbReportsApplication.class, args);
    }

}
